/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ashwinven
 */
public class VitalSignHistory {
    
    private List<VitalSigns> vitalSignHistory;
    
      public VitalSignHistory() {
      vitalSignHistory  = new ArrayList<VitalSigns>();
    }

    public List<VitalSigns> getVitalSignHistory() {
        return vitalSignHistory;
    }

    public void setVitalSignHistory(List<VitalSigns> vitalSignHistory) {
        this.vitalSignHistory = vitalSignHistory;
    }
    
    public VitalSigns addVitalSigns(){
     VitalSigns Vsigns = new VitalSigns();
     vitalSignHistory.add(Vsigns);
     return Vsigns;
    } 
    
    public void deleteVitalSigns(VitalSigns V){
        vitalSignHistory.remove(V);
    }
}
