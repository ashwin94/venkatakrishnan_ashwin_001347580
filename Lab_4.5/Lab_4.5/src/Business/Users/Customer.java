/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Users;

import Business.Abstract.User;
import Business.CustomerDirectory;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author ashwinven
 */
public class Customer extends User implements Comparable<Customer>{
    
//    private CustomerDirectory customerDirectory;
    
    public Customer(String password, String username) {
        super(password,username,"CUSTOMER");
//        customerDirectory = new CustomerDirectory();
    }
    
    @Override
    public int compareTo(Customer o) {
        return o.getUserName().compareTo(this.getUserName());
    }

    @Override
    public String toString() {
        return getUserName(); //To change body of generated methods, choose Tools | Templates.
    }
    
    public boolean verify(String password){
        if(password.equals(getPassword()))
            return true;
        return false;
    }
    
    public String currentDateTime(){
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");  
        Date date = new Date();  
        return (formatter.format(date));  
    }
    
}
