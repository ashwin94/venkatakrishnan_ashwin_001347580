/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import Business.Employee.Employee;
import Business.Organization.AdminOrganization;
import Business.Organization.DoctorOrganization;
import Business.Organization.LabOrganization;
import Business.UserAccount.UserAccount;

/**
 *
 * @author ran
 */
public class ConfigureABusiness {
    
    public static Business configure(){
        // Three roles: LabAssistant, Doctor, Admin
        
        Business business = Business.getInstance();
        
        //Create Admin organizaion
        AdminOrganization adminOrganization = new AdminOrganization();
        
        business.getOrganizationDirectory().getOrganizationList().add(adminOrganization);
        
        //Employee
        Employee employee = new Employee();
        employee.setName("Alex");
        
        //User
        UserAccount account = new UserAccount();
        account.setUsername("admin");
        account.setPassword("admin");
        account.setRole("Admin");
        account.setEmployee(employee);

        adminOrganization.getEmployeeDirectory().getEmployeeList().add(employee);
        
        adminOrganization.getUserAccountDirectory().getUserAccountList().add(account);
        
        
        //Create Lab Organization
        LabOrganization labOrganization = new LabOrganization();
        business.getOrganizationDirectory().getOrganizationList().add(labOrganization);
        
        //Lab Employee
        Employee labEmployee = new Employee();
        labEmployee.setName("Ben");
        
        //User
        UserAccount labAccount = new UserAccount();
        labAccount.setUsername("lab");
        labAccount.setPassword("lab");
        labAccount.setRole("Lab");
        labAccount.setEmployee(labEmployee);
        
        labOrganization.getEmployeeDirectory().getEmployeeList().add(labEmployee);
        labOrganization.getUserAccountDirectory().getUserAccountList().add(labAccount);
        
        
        //Create Doctor Organization
        DoctorOrganization doctorOrganization = new DoctorOrganization();
        business.getOrganizationDirectory().getOrganizationList().add(doctorOrganization);
        
        //Doctor Emplpoyee
        Employee doctorEmployee = new Employee();
        doctorEmployee.setName("Jackie");
        
        //User
        UserAccount doctorAccount = new UserAccount();
        doctorAccount.setUsername("doctor");
        doctorAccount.setPassword("doctor");
        doctorAccount.setRole("Doctor");
        doctorAccount.setEmployee(doctorEmployee);
        
        doctorOrganization.getEmployeeDirectory().getEmployeeList().add(doctorEmployee);
        doctorOrganization.getUserAccountDirectory().getUserAccountList().add(doctorAccount);
        
        return business;
    }
    
}
